//
//  KYCData.swift
//  HVZomato
//
//  Created by Yash Saxena on 18/02/21.
//

import Foundation

struct KYCData: Decodable {
    let status: String
    let result: [Result1]
}

struct Result1: Decodable {
    let details: details
}

struct details: Decodable {
    let name: name
    let father: father
    let date: date
    let pan_no: pan_no
}

struct name: Decodable{
    let value: String
}

struct father: Decodable {
    let value: String
}

struct date: Decodable {
    let value: String
}

struct pan_no: Decodable {
    let value: String
}
