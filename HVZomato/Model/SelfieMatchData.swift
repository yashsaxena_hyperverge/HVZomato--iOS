//
//  SelfieMatchData.swift
//  HVZomato
//
//  Created by Yash Saxena on 21/02/21.
//

import Foundation

struct SelfieMatchData: Decodable {
    let status: String
    let result: Result
}

struct Result: Decodable {
    let match: String
}
