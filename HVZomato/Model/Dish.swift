//
//  Dish.swift
//  HVZomato
//
//  Created by Yash Saxena on 23/02/21.
//

import Foundation

struct Dish {
    let name: String
    let isVeg: Bool
    let price: Int
}
