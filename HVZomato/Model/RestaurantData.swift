//
//  RestaurantData.swift
//  HVZomato
//
//  Created by Yash Saxena on 21/02/21.
//

import Foundation

struct RestaurantData: Decodable {
    let results_shown: Int
    let restaurants: [Restaurants]
}

struct Restaurants: Decodable {
    let restaurant: Restaurant1
}

struct Restaurant1: Decodable {
    let name: String
    let user_rating: UserRating
    let thumb: String
    let featured_image: String
}

struct UserRating: Decodable {
    let aggregate_rating: String
}
