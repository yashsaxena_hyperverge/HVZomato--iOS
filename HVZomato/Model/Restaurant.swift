//
//  Restaurant.swift
//  HVZomato
//
//  Created by Yash Saxena on 21/02/21.
//

import Foundation
import UIKit

class Restaurant {
    let name: String
    let rating: String
    let ratingDouble: Double
    var thumbnail: UIImage
    let thumbnailUrl: String
    let featuredImageUrl: String
    
    init(name: String, rating: String, thumbnailUrl: String, featuredImageUrl: String) {
        self.name = name
        self.rating = rating
        self.thumbnailUrl = thumbnailUrl
        self.thumbnail = UIImage(named: "zomato_logo.jpg")!
        self.ratingDouble = Double(rating)!
        self.featuredImageUrl = featuredImageUrl
    }
}
