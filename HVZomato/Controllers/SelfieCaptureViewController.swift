//
//  SelfieCaptureViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 19/02/21.
//

import UIKit
import CameraManager

class SelfieCaptureViewController: UIViewController {

    @IBOutlet weak var previewView: UIView!
    let cameraManager = CameraManager()
    var myImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.setHidesBackButton(true, animated: true);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cameraManager.cameraDevice = .front
        cameraManager.resumeCaptureSession()
        cameraManager.addPreviewLayerToView(self.previewView)
        cameraManager.shouldEnableTapToFocus = true
        cameraManager.shouldEnablePinchToZoom = true
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = false
        cameraManager.shouldFlipFrontCameraImage = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }
    
    @IBAction func capturePressed(_ sender: UIButton) {
        cameraManager.capturePictureWithCompletion({ [self] result in
            switch result {
                case .failure:
                    print("Error in capturing")
                case .success(let content):
                    self.myImage = content.asImage!;
                    self.performSegue(withIdentifier: "SelfieToReview", sender: self)
            }
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SelfieToReview"{
            let destination = segue.destination as! ReviewSelfieViewController
            destination.reviewImage = self.myImage
        }
    }
    
}
