//
//  KYCViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 18/02/21.
//

import UIKit
import Toast_Swift

class KYCViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fathersNameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var panNoLabel: UILabel!
    
    var name: String?
    var fathersName: String?
    var dob: String?
    var panNo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.makeToast("Identity successfully verified!")

        nameLabel.text = name
        fathersNameLabel.text = fathersName
        dobLabel.text = dob
        panNoLabel.text = panNo
    }

    @IBAction func proceedPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "KycToGoogle", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "KycToGoogle"{
            let destination = segue.destination as! GoogleSignupViewController
            destination.name = self.name
            destination.fathersName = self.fathersName
            destination.dob = self.dob
            destination.panNo = self.panNo
        }
    }
}
