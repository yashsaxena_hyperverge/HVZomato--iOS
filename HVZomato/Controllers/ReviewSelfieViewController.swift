//
//  ReviewSelfieViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 19/02/21.
//

import UIKit
import Vision
import Toast_Swift
import FirebaseStorage
import Firebase

class ReviewSelfieViewController: UIViewController {

    var reviewImage: UIImage?
    var userId: String!
    
    @IBOutlet weak var reviewImageView: UIImageView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    @IBOutlet weak var proceedButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        reviewImageView.image = reviewImage
        progressIndicator.isHidden = true
        proceedButton.isHidden = true
        
        userId = Auth.auth().currentUser!.uid
        
        let request = VNDetectFaceRectanglesRequest { (req, err) in
            if let err = err{
                print("Failed to detect faces: ",err)
                self.view.makeToast("Failed to detect faces")
                return
            }
            
            
            req.results?.forEach({ (res) in
                guard let faceObservation = res as? VNFaceObservation else{ return }
                
                DispatchQueue.main.async {
                    let view = self.createBoxView(withColor: UIColor.red)
                    view.frame = self.transformRect(fromRect: faceObservation.boundingBox, toViewRect: self.reviewImageView)
                    self.reviewImageView.addSubview(view)
                }
            })
            
            if req.results!.count > 0 {
                self.view.makeToast("Face Detected. This face will be used for Login")
                self.proceedButton.isHidden = false
            } else{
                self.view.makeToast("No Face Detected. Please Try Again")
            }
            
            
        }
        
        guard let cgImage = reviewImage?.cgImage else{return}
        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        
        do{
            try handler.perform([request])
        } catch let reqErr{
            print("Failed to perform request: ",reqErr)
            self.view.makeToast("Failed to perform Vision request")
        }
        
    }
    
    func transformRect(fromRect: CGRect , toViewRect :UIView) -> CGRect {
        
        //Convert Vision Frame to UIKit Frame
        
        var toRect = CGRect()
        toRect.size.width = fromRect.size.width * toViewRect.frame.size.width
        toRect.size.height = fromRect.size.height * toViewRect.frame.size.height
        toRect.origin.y =  (toViewRect.frame.height) - (toViewRect.frame.height * fromRect.origin.y )
        toRect.origin.y  = toRect.origin.y -  toRect.size.height
        toRect.origin.x =  fromRect.origin.x * toViewRect.frame.size.width
        return toRect
        
    }

    func createBoxView(withColor : UIColor) -> UIView {
        
        let view = UIView()
        view.layer.borderColor = withColor.cgColor
        view.layer.borderWidth = 2
        view.backgroundColor = UIColor.clear
        return view
    }

    @IBAction func proceedPressed(_ sender: UIButton) {
        let storageRef = Storage.storage().reference()
            .child("profileImages")
            .child(userId + ".jpeg")
        
        if let uploadData = self.reviewImageView.image!.jpegData(compressionQuality: 0.5){
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error)
                }
                else{
                    storageRef.downloadURL { (url, error) in
                        let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                        changeRequest?.photoURL = url?.absoluteURL
                        changeRequest?.commitChanges(completion: { (error) in
                            print(error)
                        })
                    }
                }
            }
        }
        
        // Perform Segue
        performSegue(withIdentifier: "ReviewSelfieToMain", sender: self)
    }
}
