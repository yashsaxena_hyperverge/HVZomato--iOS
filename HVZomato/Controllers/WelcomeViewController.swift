//
//  ViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 16/02/21.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

