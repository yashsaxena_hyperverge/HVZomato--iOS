//
//  GoogleSignupViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 19/02/21.
//

import UIKit
import FirebaseUI
import Firebase
import Toast_Swift

class GoogleSignupViewController: UIViewController, FUIAuthDelegate {

    var name: String?
    var fathersName: String?
    var dob: String?
    var panNo: String?
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ref = Database.database().reference()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if Auth.auth().currentUser != nil {
            // user already exists, so move to next activity
            self.view.makeToast("User already exists")
            let currUserRef = ref.child("Users").child(Auth.auth().currentUser!.uid)
            let values = ["name": name,
                          "father": fathersName,
                          "dob": dob,
                          "pan": panNo]
            
            currUserRef.updateChildValues(values)
            self.performSegue(withIdentifier: "GoogleToSelfie", sender: self)
        } else{
            // let firebase ui do its work
            let authUI = FUIAuth.defaultAuthUI()
            authUI?.delegate = self
            let providers: [FUIAuthProvider] = [FUIGoogleAuth()]
            authUI?.providers = providers
            let authViewController = authUI!.authViewController()
            self.present(authViewController, animated: true, completion: nil)

        }
    }
    
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
      // handle user and error as necessary
        if error != nil {
            // no error occured, user successfully created, take them to next activity
            let currUserRef = ref.child("Users").child(user!.uid)
            let values = ["name": name,
                          "father": fathersName,
                          "dob": dob,
                          "pan": panNo]
            
            currUserRef.updateChildValues(values)
            self.performSegue(withIdentifier: "GoogleToSelfie", sender: self)
        } else{
            self.view.makeToast("User could not be created")
        }
        
    }
    
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String?
      if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
        return true
      }
      // other URL handling goes here.
      return false
    }

}
