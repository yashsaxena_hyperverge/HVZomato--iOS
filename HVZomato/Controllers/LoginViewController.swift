//
//  LoginViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 20/02/21.
//

import UIKit
import CameraManager

class LoginViewController: UIViewController {

    @IBOutlet weak var previewView: UIView!
    let cameraManager = CameraManager()
    var myImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        
        cameraManager.cameraDevice = .front
        cameraManager.resumeCaptureSession()
        cameraManager.addPreviewLayerToView(self.previewView)
        cameraManager.shouldEnableTapToFocus = true
        cameraManager.shouldEnablePinchToZoom = true
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = false
        cameraManager.shouldFlipFrontCameraImage = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }
    
    @IBAction func capturePressed(_ sender: UIButton) {
        cameraManager.capturePictureWithCompletion({ [self] result in
            switch result {
                case .failure:
                    print("Error in capturing")
                case .success(let content):
                    self.myImage = content.asImage!;
                    self.performSegue(withIdentifier: "LoginToReview", sender: self)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoginToReview"{
            let destination = segue.destination as! LoginReviewViewController
            destination.reviewImage = self.myImage
        }
    }
    
}
