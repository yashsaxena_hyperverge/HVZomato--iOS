//
//  RestaurantViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 23/02/21.
//

import UIKit

class RestaurantViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var restaurant: Restaurant?
    var featuredImage: UIImage?
    
    var dishes: [Dish] = []
    
    var noOfItems = 0
    var totalAmount = 0
    var orderedDishes: [Dish] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        numberOfItemsLabel.text = "0 items in cart"
        totalAmountLabel.text = "₹ 0"
        
        
        // Adding dummmy data because restauraant menu dta is not available in India via
        // Zomato API
        self.dishes = []
        self.orderedDishes = []
        dishes.append(Dish(name: "Paneer Butter Masala", isVeg: true, price: 289))
        dishes.append(Dish(name: "Dal Makhani", isVeg: true, price: 239))
        dishes.append(Dish(name: "Mutton Curry", isVeg: false, price: 675))
        dishes.append(Dish(name: "Butter Chicken", isVeg: false, price: 369))
        dishes.append(Dish(name: "Chicken Rara", isVeg: false, price: 289))
        dishes.append(Dish(name: "Kadhai Paneer", isVeg: true, price: 279))
        dishes.append(Dish(name: "Chatpate Aloo", isVeg: true, price: 229))
        dishes.append(Dish(name: "Paneer Tikka", isVeg: true, price: 349))
        dishes.append(Dish(name: "Dal Fry", isVeg: true, price: 239))
        dishes.append(Dish(name: "Malai Kofta", isVeg: true, price: 279))
        dishes.append(Dish(name: "Palak Paneer", isVeg: true, price: 279))
        dishes.append(Dish(name: "Rajma", isVeg: true, price: 229))
        dishes.append(Dish(name: "Kadhai Chaap", isVeg: true, price: 279))
        dishes.append(Dish(name: "Veg Thali", isVeg: true, price: 329))
        dishes.append(Dish(name: "Non Veg Thali", isVeg: false, price: 369))
        
        
        self.title = restaurant?.name
        
        let header = StretchyTableHeaderView(frame: CGRect(x: 0, y: 0,
                                                           width: view.frame.size.width, height: view.frame.size.width))
        
        if restaurant?.featuredImageUrl != "" {
            if let imageURL = URL(string: restaurant!.featuredImageUrl) {
                DispatchQueue.global().async {
                    guard let imageData = try? Data(contentsOf: imageURL) else { return }

                    let image = UIImage(data: imageData)
                    DispatchQueue.main.async {
//                        self.restaurants[indexPath.row].thumbnail = image!
//                        cell.thumbnailImageView.image = self.restaurants[indexPath.row].thumbnail
                        self.featuredImage = image
                        header.imageView.image = self.featuredImage!
                    }
                }
            }
        }
        
        
        
        tableView.register(UINib(nibName: "DishCell", bundle: nil), forCellReuseIdentifier: "ReusableDishCell")
        
        tableView.tableHeaderView = header
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = view.bounds
    }
    
    @IBAction func nextPressed(_ sender: UIButton) {
        
        var orderString = ""
        for dish in orderedDishes {
            orderString = orderString + dish.name + "\n"
        }
        orderString = orderString + "₹ \(totalAmount)"
        
        
        let actionSheet = UIAlertController(title: "Your Order", message: orderString, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            print("Cencel pressed")
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "orderConfirmed", sender: self)
        }))
        
        present(actionSheet, animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dishes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableDishCell",for: indexPath) as! DishCell
        cell.dishNameLabel.text = dishes[indexPath.row].name
        cell.priceLabel.text = "₹\(dishes[indexPath.row].price)"
        
        if(dishes[indexPath.row].isVeg){
            cell.isVegImageView.image = UIImage(named: "veg")
        }
        else{
            cell.isVegImageView.image = UIImage(named: "non_veg")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)        
        let selectedDish = dishes[indexPath.row]
        orderedDishes.append(selectedDish)
        
        noOfItems = noOfItems + 1
        totalAmount = totalAmount + dishes[indexPath.row].price
        
        numberOfItemsLabel.text = "\(noOfItems) items in cart"
        totalAmountLabel.text = "₹ \(totalAmount)"
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let header = tableView.tableHeaderView as? StretchyTableHeaderView else{
            return
        }
        
        header.scrollViewDidScroll(scrollView: tableView)
    }
}



