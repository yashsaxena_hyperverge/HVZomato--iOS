//
//  LoginReviewViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 20/02/21.
//

import UIKit
import FirebaseStorage
import Firebase
import Toast_Swift

class LoginReviewViewController: UIViewController {
    
    var reviewImage: UIImage?
    var userId: String!
    
    @IBOutlet weak var reviewImageView: UIImageView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        reviewImageView.image = reviewImage
        progressIndicator.isHidden = true
    }
    
    @IBAction func proceedPressed(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            sender.isHidden = true
            self.progressIndicator.startAnimating()
            self.progressIndicator.isHidden = false
        }
        
        userId = Auth.auth().currentUser!.uid
        
        let storageRef = Storage.storage().reference()
            .child("profileImages")
            .child(userId + ".jpeg")
        
        storageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            if let error = error {
                self.view.makeToast("Error in reading face data")
                print(error)
            }
            else{
                let pic = UIImage(data: data!)
                // make url
                let boundary = "Boundary-\(UUID().uuidString)"
                var request = URLRequest(url: URL(string: "https://ind-faceid.hyperverge.co/v1/photo/verifyPair")!,timeoutInterval: Double.infinity)
                request.addValue("857acb", forHTTPHeaderField: "appId")
                request.addValue("e0d888e703691bccbf92", forHTTPHeaderField: "appKey")
                request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                
                let imageData1 = self.reviewImageView.image!.jpegData(compressionQuality: 0.7)! as NSData
                let imageData2 = (pic?.jpegData(compressionQuality: 0.7)!)! as NSData
                
                let imageDataDict = ["selfie" : imageData1, "selfie2" : imageData2]
                
                request.httpMethod = "POST"
                request.httpBody = self.createBodyWithParameters(filePathKey: "file", imageDataKey: imageDataDict, boundary: boundary) as Data
                
                let task = URLSession.shared.dataTask(with: request) { data, response, error in
                  guard let data = data else {
                    print(String(describing: error))
                    return
                  }
//                  print(String(data: data, encoding: .utf8)!)
                    DispatchQueue.main.async {
                        self.progressIndicator.stopAnimating()
                        self.progressIndicator.isHidden = true
                        sender.isHidden = false
                        self.parseJSON(kycData: data)
                    }
                    
                }

                task.resume()
            }
        }
    }
    
    
    func parseJSON(kycData: Data) {
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(SelfieMatchData.self, from: kycData)
            if decodedData.status == "success"{
                if decodedData.result.match == "yes"{
                    self.view.makeToast("Face Recognised!")
                    performSegue(withIdentifier: "selfieMatchToMain", sender: self)
                }
                else{
                    self.view.makeToast("Face Not Recognised")
                    return
                }
                
            }else{
                self.view.makeToast("Couldn't complete request")
            }
            
        } catch{
            print(error)
        }
    }
    
    
    func createBodyWithParameters(filePathKey: String?, imageDataKey: [String : NSData], boundary: String) -> NSData{
        let body = NSMutableData();
        
        for(k, value) in imageDataKey{
            let key = k
            let filename = "\(key).jpg"
            let mimetype = "image/jpg"
            
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(filename)\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.append(value as Data)
            body.appendString("\r\n")
            
        }
        body.appendString("--\(boundary)--\r\n")
        
        return body
    }
    
    
}
