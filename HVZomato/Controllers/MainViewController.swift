//
//  MainViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 20/02/21.
//

import UIKit
import MapKit
import Toast_Swift
import CoreLocation
import SideMenu

class MainViewController: UIViewController, MenuControllerDelegate {
        
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var restaurants: [Restaurant] = []
    var menu: SideMenuNavigationController?
    var selectedRestaurant: Restaurant? = nil
    
    let locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: true);
        navigationController?.isNavigationBarHidden = false
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mController = MenuListController(with: ["Profile",
                                                    "My Orders",
                                                    "Log Out"
        ])
        mController.delegate = self
        menu = SideMenuNavigationController(rootViewController: mController)
        menu?.leftSide = true
        SideMenuManager.default.leftMenuNavigationController = menu
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)

        activityIndicator.startAnimating()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "RestaurantCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
        checkLocationServices()
        
        // setting this controler as the delegate for locationManager
        locationManager.delegate = self
        locationManager.requestLocation()
    }
    
    func didSelectMenuItem(name: String) {
        menu?.dismiss(animated: true, completion: {
            switch name {
            case "Log Out":
                self.logout()
            default:
                self.view.makeToast("Not recognised")
            }
        })
    }
    
    func checkLocationServices() {
      if CLLocationManager.locationServicesEnabled() {
        checkLocationAuthorization()
      } else {
        // Show alert letting the user know they have to turn this on.
        self.view.makeToast("Please allow permission to access your location")
      }
        return
    }
    

    func checkLocationAuthorization() {
      switch CLLocationManager.authorizationStatus() {
      case .authorizedWhenInUse:
        mapView.showsUserLocation = true
       case .denied: // Show alert telling users how to turn on permissions
       break
      case .notDetermined:
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
      case .restricted: // Show an alert letting them know what’s up
       break
      case .authorizedAlways:
       break
      }
        return
    }
    
    
    //MARK: - Fetching Restaurants from Zomato API


    func fetchRestaurants(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
        
        let boundary = "Boundary-\(UUID().uuidString)"
        let url = URL(string: "https://developers.zomato.com/api/v2.1/search?lat=\(latitude)&lon=\(longitude)&radius=500")
        var request = URLRequest(url: url!,timeoutInterval: Double.infinity)
        request.addValue("8ee7585ee9bb2aa80d640d7171cabc63", forHTTPHeaderField: "user-key")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                self.parseJSON(restaurantData: data)
            }
        }
        
        task.resume()
    }
    
    
    func parseJSON(restaurantData: Data){
        let decoder = JSONDecoder()
        self.restaurants = []
        do{
            let decodedData = try decoder.decode(RestaurantData.self, from: restaurantData)
            if decodedData.results_shown > 0{
                //self.view.makeToast("\(decodedData.results_shown) Restaurants Found")
                for resturant in decodedData.restaurants {
                    let restaurantName = resturant.restaurant.name
                    let rating = resturant.restaurant.user_rating.aggregate_rating
                    let thumbnailImageUrl = resturant.restaurant.thumb
                    let featuredImageUrl = resturant.restaurant.featured_image
                    
                    let newRestaurant = Restaurant(name: restaurantName, rating: rating, thumbnailUrl: thumbnailImageUrl, featuredImageUrl: featuredImageUrl)
                    restaurants.append(newRestaurant)
                }
                
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
                
            }else{
                self.view.makeToast("No Restaurants Found")
            }
            
        } catch{
            print(error)
        }
    }
    
    
    func sortByRating(){
        restaurants.sort(by: ({$0.ratingDouble > $1.ratingDouble}))
        tableView.reloadData()
        self.view.makeToast("Sorted By Ratings")
    }
    
    func logout() {
        //self.view.makeToast("Inside MainViewController")
        performSegue(withIdentifier: "logoutSegue", sender: self)
    }
    
    
    @IBAction func sortPressed(_ sender: UIBarButtonItem) {
        sortByRating()
    }
    
    @IBAction func menuPressed(_ sender: UIBarButtonItem) {
        present(menu!, animated: true)
    }
}



//MARK: - CLLocationManagerDelegate


extension MainViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            // last location is most accurate
            
            locationManager.stopUpdatingLocation()
            
            let myLocation : CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            let viewRegion = MKCoordinateRegion(center: myLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            mapView.setRegion(viewRegion, animated: true)
            mapView.showsUserLocation = true
            
            let lat = location.coordinate.latitude
            let long = location.coordinate.longitude
            self.fetchRestaurants(latitude: lat, longitude: long)
        }
    
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}


extension MainViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        let restaurant = restaurants[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCell", for: indexPath) as! RestaurantCell
        cell.nameLabel.text = restaurants[indexPath.row].name
        cell.ratingLabel.text = restaurants[indexPath.row].rating
        
        if restaurants[indexPath.row].thumbnailUrl != "" {
            if let imageURL = URL(string: restaurants[indexPath.row].thumbnailUrl) {
                DispatchQueue.global().async {
                    guard let imageData = try? Data(contentsOf: imageURL) else { return }

                    let image = UIImage(data: imageData)
                    DispatchQueue.main.async {
                        self.restaurants[indexPath.row].thumbnail = image!
                        cell.thumbnailImageView.image = self.restaurants[indexPath.row].thumbnail
                    }
                }
            }
        }
        
        cell.thumbnailImageView.image = restaurants[indexPath.row].thumbnail
        
        
        
        
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        selectedRestaurant = restaurants[indexPath.row]
        
        performSegue(withIdentifier: "restaurantMenuSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "restaurantMenuSegue"{
            let destination = segue.destination as! RestaurantViewController
            destination.restaurant = self.selectedRestaurant!
        }
    }
}
