//
//  OrderConfirmedViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 23/02/21.
//

import UIKit
import Lottie

class OrderConfirmedViewController: UIViewController {

    @IBOutlet weak var successAnimationView: AnimationView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        successAnimationView.backgroundColor = .clear
        successAnimationView.play()
    }

    @IBAction func homePressed(_ sender: UIButton) {
        performSegue(withIdentifier: "homeSegue", sender: self)
    }
    
}
