//
//  ReviewRegisterViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 17/02/21.
//

import UIKit
import Foundation
import Toast_Swift

class ReviewRegisterViewController: UIViewController {
    
    var reviewImage: UIImage?
    let kycViewController = KYCViewController()
    var name: String?
    var fathersName: String?
    var dob: String?
    var panNo: String?
    
    @IBOutlet weak var reviewImageView: UIImageView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reviewImageView.image = reviewImage
        progressIndicator.isHidden = true
    }
    
    @IBAction func proceedPressed(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            sender.isHidden = true
            self.progressIndicator.startAnimating()
            self.progressIndicator.isHidden = false
        }
        
        // make url
        let boundary = "Boundary-\(UUID().uuidString)"
        var request = URLRequest(url: URL(string: "https://ind-docs.hyperverge.co/v2.0/readKYC")!,timeoutInterval: Double.infinity)
        request.addValue("857acb", forHTTPHeaderField: "appId")
        request.addValue("e0d888e703691bccbf92", forHTTPHeaderField: "appKey")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let imageData = reviewImageView.image!.jpegData(compressionQuality: 1)! as NSData
        
        request.httpMethod = "POST"
        request.httpBody = createBodyWithParameters(filePathKey: "file", imageDataKey: imageData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          //print(String(data: data, encoding: .utf8)!)
            DispatchQueue.main.async {
                self.progressIndicator.stopAnimating()
                self.progressIndicator.isHidden = true
                sender.isHidden = false
                self.parseJSON(kycData: data)
            }
            
        }

        task.resume()
        
    }
    
    func parseJSON(kycData: Data) {
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(KYCData.self, from: kycData)
            if decodedData.status == "success"{
                name = decodedData.result[0].details.name.value
                fathersName = decodedData.result[0].details.father.value
                dob = decodedData.result[0].details.date.value
                panNo = decodedData.result[0].details.pan_no.value
                
                kycViewController.name = name
                kycViewController.fathersName = fathersName
                kycViewController.dob = dob
                kycViewController.panNo = panNo
                performSegue(withIdentifier: "ReviewToKyc", sender: self)
                
            }else{
                self.view.makeToast("Identity could not be verified")
            }
            
        } catch{
            print(error)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReviewToKyc"{
            let destination = segue.destination as! KYCViewController
            destination.name = self.name
            destination.fathersName = self.fathersName
            destination.dob = self.dob
            destination.panNo = self.panNo
        }
    }
    
    func createBodyWithParameters(filePathKey: String?, imageDataKey: NSData, boundary: String) -> NSData{
        let body = NSMutableData();
        let key = "image"
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(key)\"")
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString("\r\n")
        body.appendString("--\(boundary)--\r\n")
        return body
        
    }

}

extension NSMutableData {

    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
