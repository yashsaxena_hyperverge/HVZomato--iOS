//
//  RegisterViewController.swift
//  HVZomato
//
//  Created by Yash Saxena on 17/02/21.
//

import UIKit
import CameraManager

class RegisterViewController: UIViewController {

    @IBOutlet weak var previewView: UIView!
    let cameraManager = CameraManager()
    var myImage = UIImage()
    var reviewRegisterViewController = ReviewRegisterViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
        
        cameraManager.addPreviewLayerToView(self.previewView)
        cameraManager.resumeCaptureSession()
        cameraManager.shouldEnableTapToFocus = true
        cameraManager.shouldEnablePinchToZoom = true
        cameraManager.shouldEnableExposure = true
        cameraManager.writeFilesToPhoneLibrary = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cameraManager.stopCaptureSession()
    }
    
    @IBAction func capturePressed(_ sender: UIButton) {
        cameraManager.capturePictureWithCompletion({ [self] result in
            switch result {
                case .failure:
                    print("Error in capturing")
                case .success(let content):
                    self.myImage = content.asImage!;
                    reviewRegisterViewController.reviewImage = self.myImage
                    self.performSegue(withIdentifier: "ReviewRegisterSegue", sender: self)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReviewRegisterSegue"{
            let destination = segue.destination as! ReviewRegisterViewController
            destination.reviewImage = self.myImage
        }
    }

}
